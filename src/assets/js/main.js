(function ($) {
    $(window).load(function(){
        var $invoice = $('#billing_invoice');
        if($invoice.length){
            var $billingFields = $('.woocommerce-billing-fields');
            $invoice.change(function(){
                if(this.checked){
                    $billingFields.show();
                }else{
                    $billingFields.hide();
                }
            }).change();
        }


        if($('.uap-general-date-filter').length){
            /** Traducir datepickers */
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '< Ant',
                nextText: 'Sig >',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $('.uap-general-date-filter[name="udf"]').attr('placeholder', 'Desde:');
            $('.uap-general-date-filter[name="udu"]').attr('placeholder', 'Hasta:');
        }

        var $passwords = $('input#reg_password, input#uap_pass1_field, input#uap_pass2_field, input#account_password');
        if($passwords.length){
            $passwords.attr('maxlength', '7');
            $passwords.on('focusin', function(){
                var $this = $(this);
                if(!$this.next('span.description').length)
                    $this.after('<span class="description">La contraseña debe tener 7 caracteres</span>');
            }).on('focusout', function(){
                $(this).next('span.description').remove();
            });
        }
    });
})(jQuery);