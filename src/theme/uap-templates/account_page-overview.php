<?php
$current_affiliate_id = $indeed_db->affiliate_get_id_by_uid(get_current_user_id());
?>
<div class="uap-ap-wrap">
	<?php if (!empty($data['title'])) : ?>
		<h3><?php echo $data['title']; ?></h3>
	<?php endif; ?>
	<?php if (!empty($data['stats'])) : ?>
		<div class="uap-row">
			<div class="uapcol-md-4 uap-account-overview-tab1">
				<div class="uap-account-no-box uap-account-box-green" style="padding-left:0px;">
					<div class="uap-account-no-box-inside">
						<div class="uap-count"> <?php echo $data['stats']['referrals']; ?> </div>
						<div class="uap-detail"><?php echo __('Total Referrals', 'uap'); ?></div>
						<div class="uap-subnote"><?php echo __('rewards and commissions received by now', 'uap'); ?></div>
					</div>
				</div>
			</div>
			<div class="uapcol-md-4 uap-account-overview-tab2">
				<div class="uap-account-no-box uap-account-box-lightyellow" style="padding-left:0px;">
					<div class="uap-account-no-box-inside">
						<div class="uap-count"> <?php echo $data['stats']['paid_referrals_count']; ?> </div>
						<div class="uap-detail"><?php echo __('Paid Referrals', 'uap'); ?></div>
						<div class="uap-subnote"><?php echo __('withdrawn number of referrals until now', 'uap'); ?></div>
					</div>
				</div>
			</div>
			<div class="uapcol-md-4 uap-account-overview-tab3">
				<div class="uap-account-no-box uap-account-box-red" style="padding-left:0px;">
					<div class="uap-account-no-box-inside">
						<div class="uap-count"> <?php echo $data['stats']['unpaid_referrals_count']; ?> </div>
						<div class="uap-detail"><?php echo __('UnPaid Referrals', 'uap'); ?></div>
						<div class="uap-subnote"><?php echo __('which have been not withdrawn yet', 'uap'); ?></div>
					</div>
				</div>
			</div>
			<div class="uapcol-md-4 uap-account-overview-tab4">
				<div class="uap-account-no-box uap-account-box-lightblue " style="padding-left:0px;">
					<div class="uap-account-no-box-inside">
						<div class="uap-count"> <?php echo $data['stats']['payments']; ?> </div>
						<div class="uap-detail"><?php echo __('Total Payout Transactions', 'uap'); ?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="uap-row">
			<div class="uapcol-md-3 uap-account-overview-tab7">
				<div class="uap-account-no-box uap-account-box-lightgray">
					<div class="uap-account-no-box-inside">
						<div class="uap-count">
							<?php
							$user_query = new WP_User_Query( 
								array(
									'fields'          => 'ids',
									'meta_query'    => array(
										'relation'  => 'AND',
										array( 
											'key'     => 'uap_parent_affiliate_id',
											'value'   => $current_affiliate_id,
										)
									)
								) 
							);
							echo count($user_query->get_results());
							?>
						</div>
						<div class="uap-detail"><?php echo __('Número de usuarios referidos registrados', 'uap'); ?></div>
					</div>
				</div>
			</div>
			<div class="uapcol-md-3 uap-account-overview-tab5">
				<div class="uap-account-no-box uap-account-box-lightgray">
					<div class="uap-account-no-box-inside">
						<div class="uap-count"> <?php echo uap_format_price_and_currency($data['stats']['currency'], round($data['stats']['paid_payments_value'], 2)); ?> </div>
						<div class="uap-detail"><?php echo __('Withdrawn  Earnings by Now (total Transactions)', 'uap'); ?></div>
					</div>
				</div>
			</div>
			<div class="uapcol-md-3 uap-account-overview-tab6">
				<div class="uap-account-no-box uap-account-box-blue">
					<div class="uap-account-no-box-inside">
						<div class="uap-count"> <?php echo uap_format_price_and_currency($data['stats']['currency'], round($data['stats']['unpaid_payments_value'], 2)); ?> </div>
						<div class="uap-detail"><?php echo __('Current Account Balance', 'uap'); ?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="uap-profile-box-wrapper">
			<div class="uap-profile-box-content">
				<div class="uap-row ">
					<div class="uap-col-xs-12">
						<div class="uap-account-help-link">
							<?php _e('You can learn more about Affiliate program and to start earning referrals ', 'uap'); ?>
							<a href="<?php echo $data['help_url']; ?>">
								<?php _e('here', 'uap'); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="uap-public-general-stats">
		<div><?php echo __('Total number of Referrals:') . $data['stats']['referrals']; ?></div>
		<div><?php echo __('Total number of Payments:') . $data['stats']['payments']; ?></div>
		<div><?php echo __('Total number of Paid Referrals:') . $data['stats']['paid_referrals_count']; ?></div>
		<div><?php echo __('Total number of UnPaid Referrals:') . $data['stats']['unpaid_referrals_count']; ?></div>
		<div><?php echo __('Total value of Paid Payments:') . uap_format_price_and_currency($data['stats']['currency'], round($data['stats']['paid_payments_value'], 2)); ?></div>
		<div><?php echo __('Total value of Unpaid Payments:') . uap_format_price_and_currency($data['stats']['currency'], round($data['stats']['unpaid_payments_value'], 2)); ?></div>
	</div> -->
	<?php endif; ?>

	<?php if (!empty($data['message'])) : ?>
		<p><?php echo do_shortcode($data['message']); ?></p>
	<?php endif; ?>
</div>