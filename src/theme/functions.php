<?php

/** analytics */
require_once(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require_once(get_stylesheet_directory().'/includes/store-changes.php');

/** funciones de búsqueda */
require_once(get_stylesheet_directory().'/includes/search-results.php');

/** sobreescribir grid de productos */
require_once(get_stylesheet_directory().'/includes/etheme-product.php');

/** sobreescribir lista y carrusel de marcas */
require_once(get_stylesheet_directory().'/includes/etheme-brands.php');
require_once(get_stylesheet_directory().'/includes/etheme-brand-list.php');

/** Agregar filtros de categorías */
require_once(get_stylesheet_directory().'/includes/category-filter.php');
register_widget('ETC\App\Models\Widgets\Categories_Filter');

/** funciones de descuentos */
require_once(get_stylesheet_directory().'/includes/mt-discounts.php');

/** cambios del sistema de afiliados */
require_once(get_stylesheet_directory().'/includes/uap-changes.php');

add_action('wp_enqueue_scripts', 'mt_resources');
function mt_resources(){
    etheme_child_styles();
    wp_register_script('main-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
    // wp_localize_script('main-functions', 'mainGlobalObject', array(
	// 	'add_cart_button_script' => is_shop() || is_product_category() || is_product_tag(),
	// ));
    wp_enqueue_script('main-functions');
    // if (is_front_page() || is_single() ) wp_dequeue_script('wc-cart-fragments');
}

//disable zxcvbn.min.js in wordpress
add_action('wp_print_scripts', 'mt_remove_password_strength_meter');
function mt_remove_password_strength_meter() {
    // Deregister script about password strenght meter
    wp_dequeue_script('zxcvbn-async');
    wp_deregister_script('zxcvbn-async');
    
    wp_dequeue_script('wc-password-strength-meter');
    wp_deregister_script('wc-password-strength-meter');
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'mt_theme_lang' );
function mt_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}

/**
 * Whatsapp
 */
add_action('wp_footer','mt_add_footer_whatsapp');
function mt_add_footer_whatsapp(){
	$tel = "525567837271";

	$url = "https://wa.me/${tel}";
	$img = get_stylesheet_directory_uri().'/img/whatsapp-icon.svg';
	echo "<div id='float-whatsapp'>";
	echo "<a href=${url} target='_blank'>";
	echo "<img width='40' height='40' id='float-whatsapp-img' src='${img}' alt='whatsapp-icon' />";
	echo " </a>";
	echo "</div>";
}

/**
 * Reescribe etiquetas
 */
add_filter( 'gettext', 'mt_change_translation_strings', 20, 3 );
function mt_change_translation_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Shopping Cart' :
            $translated_text = __( 'Carrito', $domain );
        break;
        case 'My Wishlist' :
            $translated_text = __( 'Favoritos', $domain );
        break;
        case 'All brands' :
            $translated_text = __( 'Todas las marcas', $domain );
        break;
        case 'From' :
            $translated_text = __( 'De', $domain );
        break;
        case 'Shipping to %s.' :
            $translated_text = __( 'Enviar a %s.', $domain );
        break;
        case 'Change address' :
            $translated_text = __( 'Cambiar dirección', $domain );
        break;
    }
    return $translated_text;
}
