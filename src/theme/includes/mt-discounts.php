<?php
if (!class_exists('MT_Discounts')){
    class MT_Discounts{
        public $user_id = '';
        public $user_rank = '';
        public $cart_items = 0;
        public $available_discounts = 0;
        public $discounts_rates = array(
            'intermedio' => array(
                'items' => 1,
                'discount' => 0.1
            ),
            'avanzado' => array(
                'items' => 2,
                'discount' => 0.2
            ),
            'experto' => array(
                'items' => 3,
                'discount' => 0.3
            ),
        );

        public function __construct(){
            global $indeed_db;
            $this->user_id = get_current_user_id();

            $affiliate_id = $indeed_db->get_affiliate_id_by_wpuid($this->user_id);
            $rank = $indeed_db->get_affiliate_rank($affiliate_id);
            if ($rank){
                $rank_data = $indeed_db->get_rank($rank);
                $rank_name = (empty($rank_data['slug'])) ? '' : $rank_data['slug'];
                $this->user_rank = $rank_name;
            }
           
            add_filter( 'woocommerce_order_data_store_cpt_get_orders_query', array($this, 'handle_custom_query_var'), 10, 2 );
            add_action('woocommerce_before_calculate_totals', array($this, 'get_available_discounts'), 10 );

            add_action('woocommerce_cart_calculate_fees', array($this, 'calculate_discount'), 10 );
            add_action('woocommerce_proceed_to_checkout', array($this, 'set_messages'), 10 );
            add_action('woocommerce_review_order_before_payment', array($this, 'set_messages'), 10 );
        }
        
        public function calculate_discount(WC_Cart $cart){
            if( !$this->user_rank ) return;
            
            $this->cart_items = $cart->get_cart_contents_count();
            if($this->available_discounts > 0 && $this->cart_items <= $this->available_discounts){
                $discount = $cart->subtotal * $this->discounts_rates[$this->user_rank]['discount'];
                $cart->add_fee( 'Descuento por rango '.$this->user_rank, -$discount);
                add_action('woocommerce_checkout_create_order', array($this, 'set_discount_order_meta'), 10, 2);
            }
        }

        public function set_discount_order_meta($order, $data){
            $order->update_meta_data( '_mt_rank_discount_applied', 1 );
        }

        public function set_messages(){
            if($this->user_rank):?>
                <?php if($this->available_discounts > 0 && $this->cart_items <= $this->available_discounts): ?>
                    <p style="color:red">Se usarán <strong><?php echo $this->cart_items ?></strong> de tus <strong><?php echo $this->available_discounts ?> descuentos disponibles este mes.</strong></p>
            
                <?php elseif($this->available_discounts > 0 && $this->cart_items > $this->available_discounts): ?>
                    <p style="color:red">Actualmente tienes <strong><?php echo $this->available_discounts ?> descuentos disponibles</strong> para este mes. Para aplicarlo automáticamente reduce tus artículos del carrito</p>
                    
                <?php elseif($this->available_discounts <= 0): ?>
                    <p style="color:red">No tienes descuentos para este mes.</p>
                <?php endif; ?>

                <p style="font-size: 1.15rem; text-align: justify; border: 1px dashed black; padding: 5px 15px">Actualmente cuentas con <strong><?php echo $this->discounts_rates[$this->user_rank]['items'] ?> descuentos por mes</strong> por tu rango <strong><?php echo $this->user_rank; ?></strong>. Tus descuentos son del <strong><?php echo $this->discounts_rates[$this->user_rank]['discount']*100 ?>%</strong></p>
            <?php endif;
        }

        public function get_available_discounts(){

            if(!array_key_exists($this->user_rank, $this->discounts_rates)) return;
            
            $args = array(
                'status' => ['processing', 'on-hold', 'completed'],
                'mt_rank_discount_applied' => 1,
                'limit' => -1,
                'customer_id' => get_current_user_id(),
                'date_created' => 'this month'
            );
            $orders = wc_get_orders( $args );
    
            $purchased_items = 0;
            foreach($orders as $order){
                $purchased_items += count( $order->get_items());
            }

            $available_discounts = $this->discounts_rates[$this->user_rank]['items'] - $purchased_items > 0 ? $this->discounts_rates[$this->user_rank]['items'] - $purchased_items : 0;
            
            $this->available_discounts = $available_discounts;
        }

       function handle_custom_query_var( $query, $query_vars ) {
            if ( ! empty( $query_vars['mt_rank_discount_applied'] ) ) {
                $query['meta_query'][] = array(
                    'key' => '_mt_rank_discount_applied',
                    'value' => esc_attr( $query_vars['mt_rank_discount_applied'] ),
                );
            }
            return $query;
        }
    }
}

new MT_Discounts();