<?php
add_filter( 'woocommerce_redirect_single_search_result', '__return_false' );

add_filter( 'posts_search', 'mt_search_custom_query', 500, 2 );
function mt_search_custom_query( $search, $wp_query ){
	global $wpdb;
        
	if ( empty( $search ) || !empty($wp_query->query_vars['suppress_filters']) ) {
		return $search; // skip processing - If no search term in query or suppress_filters is true
	}

	$q = $wp_query->query_vars;
	$n = !empty($q['exact']) ? '' : '%';
	$search = $searchand = '';

	foreach ((array)$q['search_terms'] as $term ) {        
		$term = $n . $wpdb->esc_like( $term ) . $n;

		/* change query as per plugin settings */
		$OR = '';
		
		$search .= "{$searchand} (";

		// Name
		$search .= $wpdb->prepare("($wpdb->posts.post_title LIKE '%s')", $term);
		$OR = ' OR ';
		
		// Content
		$search .= $OR;
		$search .= $wpdb->prepare("($wpdb->posts.post_content LIKE '%s')", $term);
		$OR = ' OR ';
		
		// Excerpt
		$search .= $OR;
		$search .= $wpdb->prepare("($wpdb->posts.post_excerpt LIKE '%s')", $term);
		$OR = ' OR ';

		// Meta_keys
		// $meta_key_OR = '';
		// $meta_keys = array();
		// foreach ($meta_keys as $key_slug) {
		// 	$search .= $OR;
		// 	$search .= $wpdb->prepare("$meta_key_OR (espm.meta_key = '%s' AND espm.meta_value LIKE '%s')", $key_slug, $term);
		// 	$OR = '';
		// 	$meta_key_OR = ' OR ';
		// }
		// $OR = ' OR ';
	
		// Taxonomies
		$tax_OR = '';
		$taxonomies = array('product_cat', 'product_tag', 'brand', 'pa_linea-de-producto');
		foreach ($taxonomies as $tax) {
			$search .= $OR;
			$search .= $wpdb->prepare("$tax_OR (estt.taxonomy = '%s' AND est.name LIKE '%s')", $tax, $term);
			$OR = '';
			$tax_OR = ' OR ';
		}

		$search .= ")";
		$searchand = " AND ";
	}

	if ( ! empty( $search ) ) {
		$search = " AND ({$search}) ";
		if ( ! is_user_logged_in() )
			$search .= " AND ($wpdb->posts.post_password = '') ";
	}

	 /* Join Table */
	 add_filter('posts_join_request', 'mt_search_join_table');

	 /* Request distinct results */
	 add_filter('posts_distinct_request', 'mt_search_distinct');

	return apply_filters('mt_posts_search', $search, $wp_query);
	
}

function mt_search_join_table($join){
	global $wpdb;
	// Meta keys
	// $join .= " LEFT JOIN $wpdb->postmeta espm ON ($wpdb->posts.ID = espm.post_id) ";

	// Taxonomies
	$join .= " LEFT JOIN $wpdb->term_relationships estr ON ($wpdb->posts.ID = estr.object_id) ";
	$join .= " LEFT JOIN $wpdb->term_taxonomy estt ON (estr.term_taxonomy_id = estt.term_taxonomy_id) ";
	$join .= " LEFT JOIN $wpdb->terms est ON (estt.term_id = est.term_id) ";
	return $join;
}

function mt_search_distinct($distinct) {
	$distinct = 'DISTINCT';
	return $distinct;
}