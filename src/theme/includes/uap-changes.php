<?php
/**
 * Shortcode condicional para mostrar boton de convertirse en afiliado o formulario de registro
 */
add_shortcode( 'mt_uap_register', 'mt_uap_register' );
function mt_uap_register( $atts, $content = null ) {
	$user = wp_get_current_user();
	if( array_intersect($user->roles, array('customer')) ) {
		return '<p style="text-align:center; font-size: 1.5em;"><strong>Ya te encuentras registrado</strong></p>'.do_shortcode('[uap-user-become-affiliate]');
	}else{
		return '<p style="text-align:center; font-size: 1.5em;"><strong>Conviértete en socio</strong></p>'.do_shortcode('[uap-register]');
	}
}

/**
 * Enviar correo de validación de email al afiliarse
 */
add_action( 'uap_on_register_action', 'mt_send_new_user_email');
function mt_send_new_user_email($user_id){
	//set simple username
	$user = get_userdata($user_id);
	$username = strstr($user->user_email, '@', true);
	if (username_exists($username)) {
		$suffix = 2;
		while (username_exists($username)) {
			$username = $username . '-' . $suffix;
			$suffix++;
		}
	}

	global $wpdb;
	$wpdb->update($wpdb->users, array('user_login' => $username), array('ID' => $user_id));

	//send register user
	$wc_emails = new WC_Emails();
	$wc_emails->customer_new_account($user_id);

}

/**
 * Agregar mensajes de registro de afiliados
 */
add_filter('woocommerce_before_customer_login_form', 'mt_uap_verification_message', 10, 3);
function mt_uap_verification_message() {
	if (isset($_GET['uap_register']) && $_GET['uap_register'] == 'create_message') {
        ?>
        <div class="woocommerce-message">
            <strong>Gracias:</strong> Tu cuenta ha sido creada correctamente, por favor inicia sesión en la parte de abajo con tu información.
        </div><?php
    }
}

/**
 * Activar opción oculta de comisión por precio de producto en MLM
 */

add_filter('uap_is_magic_feat_active_filter', 'mt_enable_mlmpp', 10, 2);

function mt_enable_mlmpp($isActive, $type){
	return $type == 'uap_mlmpp' ? true : $isActive;
}

/**
 * Cargar templates de child theme
 */

add_filter('uap_filter_on_load_template', 'mt_uap_templates', 10, 2);

function mt_uap_templates($fullPath, $searchFilename){
    $childTemplate = get_stylesheet_directory().'/uap-templates/'.$searchFilename;
    return file_exists($childTemplate) ? $childTemplate : $fullPath;
}

/**
 * Shortcode para mostrar icono con referidos de usuario
 */
add_shortcode( 'mt_uap_user_referrals', 'mt_uap_user_referrals' );
function mt_uap_user_referrals( $atts, $content = null ) {
    global $indeed_db;
    $user_id = get_current_user_id();
    $affiliate_id = $indeed_db->get_affiliate_id_by_wpuid($user_id);
    $rank = $indeed_db->get_affiliate_rank($affiliate_id);
    if( !$rank ) return;
    
    $uap_url = wc_get_page_permalink( 'myaccount' ).'uap';
    $start = date('Y-m-01 00:00:00');
    $end = date('Y-m-t 23:59:59');
    $referrals = $indeed_db->get_referral_report_by_date($affiliate_id, $start, $end);
    ?>
    <div class="et_element et_b_header-wishlist  flex align-items-center wishlist-type1  et-quantity-top et-content-right  et_element-top-level">
        <a href="<?php echo $uap_url; ?>" class=" flex flex-wrap full-width align-items-center  justify-content-end mob-justify-content-end">
            <span class="flex-inline justify-content-center align-items-center flex-wrap">
                <span class="et_b-icon">
                    <span class="et-svg"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M437 75C388.7 26.6 324.4 0 256 0S123.3 26.6 75 75C26.6 123.3 0 187.6 0 256s26.6 132.7 75 181C123.3 485.4 187.6 512 256 512s132.7-26.6 181-75C485.4 388.7 512 324.4 512 256S485.4 123.3 437 75zM256 482C131.4 482 30 380.6 30 256S131.4 30 256 30s226 101.4 226 226S380.6 482 256 482z"/><path d="M272.1 241h-32.1c-18.3 0-33.2-14.9-33.2-33.2 0-18.3 14.9-33.2 33.2-33.2H304.2c8.3 0 15-6.7 15-15s-6.7-15-15-15H271v-33.2c0-8.3-6.7-15-15-15s-15 6.7-15 15v33.2h-1.1c-34.8 0-63.2 28.4-63.2 63.2 0 34.8 28.4 63.2 63.2 63.2h32.1c18.3 0 33.2 14.9 33.2 33.2 0 18.3-14.9 33.2-33.2 33.2h-64.3c-8.3 0-15 6.7-15 15s6.7 15 15 15H241v33.2c0 8.3 6.7 15 15 15s15-6.7 15-15V367.4h1.1c34.8 0 63.2-28.4 63.2-63.2S306.9 241 272.1 241z"/></svg></span>
                    <span class="et-wishlist-quantity et-quantity count-0">
                        <?php echo $referrals['total_referrals']; ?>
                    </span>
                </span>
            </span>
        </a>
    </div>
    <?php
}

add_action( 'woocommerce_register_form', 'mt_add_register_ref_code_field' );
add_action( 'woocommerce_after_checkout_registration_form', 'mt_add_register_ref_code_field' ); 
function mt_add_register_ref_code_field(){
    $parent_id = '';
    if(isset($_COOKIE['uap_referral'])){
        $uap_referral = unserialize(stripslashes( $_COOKIE['uap_referral'] ));
        $parent_id = isset($uap_referral['affiliate_id']) ? $uap_referral['affiliate_id'] : '';
    }

	woocommerce_form_field(
		'parent_affiliate_id',
		array(
			'type'        => 'text',
			'required'    => false,
            'label'       => 'Código de referencia de afiliado',
            'description' => 'Si estás siendo referenciado por otra persona inserta aquí su código. Si usaste una URL de referencia su código aparecerá aquí automáticamente',
            'class'        => array( 'form-row-wide' ),
        ),
        $parent_id
	);
 
}

add_action( 'woocommerce_register_post', 'mt_validate_ref_code', 5, 3 );
function mt_validate_ref_code( $username, $email, $errors ) {
    global $indeed_db;
    $parent_id = isset($_POST['parent_affiliate_id']) ? $_POST['parent_affiliate_id'] : 0;

	if($parent_id && !$indeed_db->is_affiliate_active($parent_id)){
        $errors->add('parent_affiliate_id_error', 'El código de afiliado no existe');
    }elseif($parent_id && $indeed_db->is_affiliate_active($parent_id)){
        $data = array();
        $data['affiliate_id'] = $parent_id;
        $data['campaign'] = '';
        $data['timestamp'] = time();
        $data['site_referer'] = (empty($_SERVER['HTTP_REFERER'])) ? '' : $_SERVER['HTTP_REFERER'];
        $cookie_time = $data['timestamp'] + 24 * 60 * 60;
        
        setcookie('uap_referral', serialize($data), $cookie_time, '/'); /// name, value, expire, path
    }
}

add_action( 'woocommerce_before_checkout_process', 'mt_validate_checkout_ref_code', 5);
function mt_validate_checkout_ref_code( ){
    global $indeed_db;
    $parent_id = isset($_POST['parent_affiliate_id']) ? $_POST['parent_affiliate_id'] : 0;
    
	if($parent_id && $indeed_db->is_affiliate_active($parent_id)){
        add_filter('uap_set_affiliate_id_filter', function(){
            return $_POST['parent_affiliate_id'];
        });
    }
}

add_action('user_register', 'mt_set_affiliate_parent_metadata');
function mt_set_affiliate_parent_metadata( $user_id ) {
    $parent_id = '';
    if(isset($_COOKIE['uap_referral'])){
        $uap_referral = unserialize(stripslashes( $_COOKIE['uap_referral'] ));
        $parent_id = isset($uap_referral['affiliate_id']) ? $uap_referral['affiliate_id'] : '';
    }elseif(isset($_POST['parent_affiliate_id'])){
        $parent_id = $_POST['parent_affiliate_id'];
    }

    if($parent_id){
        update_user_meta( $user_id, 'uap_parent_affiliate_id', $parent_id );
    }
}