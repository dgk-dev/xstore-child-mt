<?php
//Google analytics tag
add_action('wp_head', 'mt_google_analytics', 7);
function mt_google_analytics(){
?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-VB32NQ89E8"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-VB32NQ89E8');
  </script>
<?php
}

/**
 * Funciones avanzadas de google analytics para ecommerce
 * 
 */

//Transacción completa
add_action('woocommerce_thankyou', 'mt_ga_add_transaction', 10, 1);
function mt_ga_add_transaction($order_id){
  $order = wc_get_order($order_id);

  if (!$order || get_post_meta($order->get_id(), '_ga_tracked', true) == 1) {
    return;
  }
  $items = array();
  if ($order->get_items()) {
    foreach ($order->get_items() as $item) {
      $items[] = mt_ga_add_item($item);
    }
  }

  $ga_code = "gtag( 'event', 'purchase',  {";
  $ga_code .= "'transaction_id': '" . esc_js($order->get_id()) . "',";
  $ga_code .= "'value': '" . esc_js($order->get_total()) . "',";
  $ga_code .= "'currency': '" . esc_js($order->get_currency()) . "',";
  $ga_code .= "'items':" . json_encode($items);
  $ga_code .= "} );";


  // Mark the order as tracked.
  update_post_meta($order_id, '_ga_tracked', 1);
  echo "<script>" . $ga_code . "</script>";
}

function mt_ga_add_item($item){
  $product = $item->get_product();
  $categories = array();
  $category_terms =  get_the_terms($item->get_product_id(), 'product_cat');
  foreach ($category_terms as $category) {
    $categories[] = $category->name;
  }

  $brands = array();
  $brand_terms =  get_the_terms($item->get_product_id(), 'brand');
  foreach ($brand_terms as $brand) {
    $brands[] = $brand->name;
  }

  $size = $item->is_type('variation') && $item->get_attribute('talla') ? $item->get_attribute('talla') : 'NA';

  $_item = array(
    'id' => $product->get_sku() ? $product->get_sku() : ('#' . $item->get_product_id()),
    'name' => $item->get_name(),
    'brand' => implode(', ', $brands),
    'category' => implode(', ', $categories),
    'variation' => $size,
    'quantity' =>  $item->get_quantity(),
    'price' => $item->get_total()
  );

  return $_item;
}


// //Agregar al carrito
add_action('woocommerce_after_add_to_cart_button', 'mt_ga_add_to_cart');
function mt_ga_add_to_cart(){
  global $product;

  $categories = array();
  $category_terms =  get_the_terms($product->get_id(), 'product_cat');
  foreach ($category_terms as $category) {
    $categories[] = $category->name;
  }

  $brands = array();
  $brand_terms =  get_the_terms($product->get_id(), 'brand');
  foreach ($brand_terms as $brand) {
    $brands[] = $brand->name;
  }

  $ga_code = "gtag( 'event', 'add_to_cart', {";
  $ga_code .= "'items': [{";
  $ga_code .= "'id': '" . esc_js($product->get_sku() ? $product->get_sku() : ('#' . $product->get_id())) . "',";
  $ga_code .= "'name': '" . esc_js($product->get_title()) . "',";
  $ga_code .= "'brand': '" . esc_js(implode(', ', $brands)) . "',";
  $ga_code .= "'category': '" . esc_js(implode(', ', $categories)) . "',";
  $ga_code .= "'variant': $( 'ul[data-attribute=\"pa_talla\"]' ).length ? $( 'ul[data-attribute=\"pa_talla\"] li.selected .st-custom-attribute' ).attr('data-name') : 'NA'" . ",";
  $ga_code .= "'price': '" . esc_js($product->get_price()) . "',";
  $ga_code .= "'quantity': $( 'input.qty' ).length ? $( 'input.qty' ).val() : '1'" . ",";
  $ga_code .= "} ] } );";

  $selector = '.single_add_to_cart_button';
  mt_ga_set_add_to_cart_code($selector, $ga_code);
}



add_action('wp_footer', 'mt_ga_add_to_cart_loop');
function mt_ga_add_to_cart_loop(){
?>
  <script>
    (function($) {
      $(document.body).on('added_to_cart', function(event, fragments, cart_hash, button) {
        variation_id = $(button[0]).parents('.product-details').find('.st-swatch-in-loop').attr('data-variation_id');

        $.ajax({
          type: "post",
          url: "<?php echo admin_url('admin-ajax.php') ?>",
          data: {
            action: 'mt_get_variation_data',
            variation_id: variation_id
          },
          dataType: 'json',
          success: function(result) {
            if (result.success) {
              gtag('event', 'add_to_cart', {
                items: [{
                  id: result.id,
                  name: result.name,
                  brand: result.brand,
                  category: result.category,
                  variation: result.variation,
                  price: result.price,
                  quantity: result.quantity
                }]
              });
            }
          }
        });
      });

    })(jQuery);
  </script>
<?php
}

add_action('wp_ajax_nopriv_mt_get_variation_data', 'mt_get_variation_data');
add_action('wp_ajax_mt_get_variation_data', 'mt_get_variation_data');

function mt_get_variation_data(){
  $product = new WC_Product_Variation($_POST['variation_id']);
  if (!$product) {
    echo json_encode(array('success' => false));
    wp_die();
  }

  $categories = array();
  $category_terms =  get_the_terms($product->get_parent_id(), 'product_cat');
  foreach ($category_terms as $category) {
    $categories[] = $category->name;
  }

  $brands = array();
  $brand_terms =  get_the_terms($product->get_parent_id(), 'brand');
  foreach ($brand_terms as $brand) {
    $brands[] = $brand->name;
  }

  $size = $product->is_type('variation') && $product->get_attribute('talla') ? $product->get_attribute('talla') : 'NA';

  $item = array(
    'success' => true,
    'id' => $product->get_sku() ? $product->get_sku() : ('#' . $product->get_parent_id()),
    'name' => $product->get_name(),
    'brand' => implode(', ', $brands),
    'category' => implode(', ', $categories),
    'variation' => $size,
    'quantity' =>  1,
    'price' => $product->get_regular_price()
  );
  echo json_encode($item);
  wp_die();
}

function mt_ga_set_add_to_cart_code($selector, $ga_code){
?>
  <script>
    (function($) {
      $(document.body).on('click', '<?php echo $selector ?>', function(e) {
        <?php echo $ga_code ?>
      });
    })(jQuery);
  </script>
<?php
}

// //Quitar del carrito
add_filter('woocommerce_cart_item_remove_link', 'filter_woocommerce_cart_item_remove_link', 10, 2);
function filter_woocommerce_cart_item_remove_link($remove_link, $cart_item_key){
  if (!is_cart()) return $remove_link;
  global $woocommerce;
  $item = $woocommerce->cart->get_cart_item($cart_item_key);
  $remove_link = str_replace('<a', '<a data-product_id="' . $item['product_id'] . '"', $remove_link);

  return $remove_link;
};
add_action('woocommerce_after_cart', 'mt_ga_remove_from_cart');
add_action('woocommerce_after_mini_cart', 'mt_ga_remove_from_cart');
function mt_ga_remove_from_cart(){
  global $woocommerce;
  $items = $woocommerce->cart->get_cart();
  $cart = array();
  foreach ($items as $item) {
    $item_data = $item['data'];
    $categories = array();
    $category_terms =  get_the_terms($item['product_id'], 'product_cat');
    foreach ($category_terms as $category) {
      $categories[] = $category->name;
    }

    $brands = array();
    $brand_terms =  get_the_terms($item['product_id'], 'brand');
    foreach ($brand_terms as $brand) {
      $brands[] = $brand->name;
    }

    $size = $item_data->is_type('variation') && $item_data->get_attribute('talla') ? $item_data->get_attribute('talla') : 'NA';

    $_item = array(
      'id' => $item_data->get_sku() ? $item_data->get_sku() : ('#' . $item['id']),
      'name' => $item_data->get_name(),
      'brand' => implode(', ', $brands),
      'category' => implode(', ', $categories),
      'variation' => $size,
      'quantity' =>  $item['quantity'],
      'price' => $item_data->get_regular_price()
    );
    $cart[$item['product_id']] = $_item;
  }
?>
  <script>
    (function($) {
      cart_items = JSON.parse('<?php echo json_encode($cart); ?>');
      removed_items = [];
      $(document.body).on('click', '.remove, .remove-item', function() {
        id = $(this).data('product_id');
        if (removed_items.includes(id)) return;
        console.log('remove');
        gtag('event', 'remove_from_cart', {
          'id': cart_items[id]['id'],
          'name': cart_items[id]['name'],
          'brand': cart_items[id]['brand'],
          'category': cart_items[id]['category'],
          'variation': cart_items[id]['variation'],
          'quantity': cart_items[id]['quantity'],
          'price': cart_items[id]['price']
        });
        removed_items.push(id);
      });
    })(jQuery);
  </script>
<?php
}

/**
 * begin_checkout
 */

add_action('woocommerce_after_cart', 'mt_ga_begin_checkout');
function mt_ga_begin_checkout(){
  mt_set_checkout_ga_code('begin_checkout');
}
add_action('woocommerce_after_checkout_form', 'mt_ga_checkout_progress');
function mt_ga_checkout_progress(){
  mt_set_checkout_ga_code('checkout_progress');
}

function mt_set_checkout_ga_code($event){
  global $woocommerce;
  $items = $woocommerce->cart->get_cart();
  $cart = array();
  foreach ($items as $item) {
    $item_data = $item['data'];

    $categories = array();
    $category_terms =  get_the_terms($item['product_id'], 'product_cat');
    foreach ($category_terms as $category) {
      $categories[] = $category->name;
    }

    $brands = array();
    $brand_terms =  get_the_terms($item['product_id'], 'brand');
    foreach ($brand_terms as $brand) {
      $brands[] = $brand->name;
    }

    $size = $item_data->is_type('variation') && $item_data->get_attribute('talla') ? $item_data->get_attribute('talla') : 'NA';

    $_item = array(
      'id' => $item_data->get_sku() ? $item_data->get_sku() : ('#' . $item['id']),
      'name' => $item_data->get_name(),
      'brand' => implode(', ', $brands),
      'category' => implode(', ', $categories),
      'variation' => $size,
      'quantity' =>  $item['quantity'],
      'price' => $item_data->get_regular_price()
    );
    $cart[] = $_item;
  }
  $ga_code = "gtag( 'event', '" . $event . "',  {";
  $ga_code .= $event == 'checkout_progress' ? "'checkout_step': 2," : '';
  $ga_code .= "'items':" . json_encode($cart);
  $ga_code .= "} );";
  echo '<script>' . $ga_code . '</script>';
}

// //Vista de detalle de producto
add_action('woocommerce_after_single_product', 'mt_ga_product_detail');
function mt_ga_product_detail(){
  global $product;
  if (empty($product)) {
    return;
  }

  $categories = array();
  $category_terms =  get_the_terms($product->get_id(), 'product_cat');
  foreach ($category_terms as $category) {
    $categories[] = $category->name;
  }

  $brands = array();
  $brand_terms =  get_the_terms($product->get_id(), 'brand');
  foreach ($brand_terms as $brand) {
    $brands[] = $brand->name;
  }

  wc_enqueue_js("
    gtag( 'event', 'view_item', {
      'id': '" . esc_js($product->get_sku() ? $product->get_sku() : ('#' . $product->get_id())) . "',
      'name': '" . esc_js($product->get_title()) . "',
      'brand': '" . esc_js(implode(', ', $brands)) . "',
      'category': '" . esc_js(implode(', ', $categories)) . "',
      'price': '" . esc_js($product->get_price()) . "',
    } );");
}

/**
 * Custom Titles and descriptions 
 */

add_filter('wpseo_opengraph_title', 'mt_custom_seo_titles');
add_filter('wpseo_title', 'mt_custom_seo_titles');
function mt_custom_seo_titles($title){
  if (is_shop()) {
    $type = isset($_GET['filter_tipo']) ? $_GET['filter_tipo'] : '';
    $category = isset($_GET['product_cat']) ? $_GET['product_cat'] : '';

    switch ($type) {
      case 'mujer':
        switch ($category) {
          case 'basketball':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Basketball para mujeres';
            break;
          case 'relajado':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Relajado para mujeres';
            break;
          case 'formal':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Formal para mujeres';
            break;
          case 'running':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Running para mujeres';
            break;
          case 'sandalias':
            $title = 'MoneyTime | Tienda en línea de Tenis | Sandalias para mujeres';
            break;
          case 'training':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de entrenamiento para mujeres';
            break;
          case 'skate':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Skate para mujeres';
            break;
          case 'casual':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Casual para mujeres';
            break;
          default:
            $title = 'MoneyTime | Tienda en línea de Tenis | Modelos para mujer';
            break;
        }
        break;
      case 'hombre':
        switch ($category) {
          case 'basketball':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Basketball para hombres';
            break;
          case 'relajado':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Relajado para hombres';
            break;
          case 'formal':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Formal para hombres';
            break;
          case 'running':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Running para hombres';
            break;
          case 'sandalias':
            $title = 'MoneyTime | Tienda en línea de Tenis | Sandalias para hombres';
            break;
          case 'training':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de entrenamiento para hombres';
            break;
          case 'skate':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Skate para hombres';
            break;
          case 'casual':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Casual para hombres';
            break;
          default:
            $title = 'MoneyTime | Tienda en línea de Tenis | Modelos para hombres';
            break;
        }
        break;
      case 'juvenil':
        switch ($category) {
          case 'basketball':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Basketball para niños';
            break;
          case 'relajado':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Relajado para niños';
            break;
          case 'formal':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Formal para niños';
            break;
          case 'running':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de Running para niños';
            break;
          case 'sandalias':
            $title = 'MoneyTime | Tienda en línea de Tenis | Sandalias para niños';
            break;
          case 'training':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado de entrenamiento para niños';
            break;
          case 'skate':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Skate para niños';
            break;
          case 'casual':
            $title = 'MoneyTime | Tienda en línea de Tenis | Calzado Casual para niños';
            break;
          default:
            $title = 'MoneyTime | Tienda en línea de Tenis | Temporada para juveniles';
            break;
        }
        break;
    }

    $brand = isset($_GET['filter_brand']) ? $_GET['filter_brand'] : '';

    switch ($brand) {
      case 'adidas':
        $title = 'MoneyTime | Tienda en línea de Calzado | Marca Adidas';
        break;
      case 'dc':
        $title = 'MoneyTime | Tienda en línea de Calzado | Marca DC';
        break;
      case 'nike':
        $title = 'MoneyTime | Tienda en línea de Calzado | Marca Nike';
        break;
      case 'puma':
        $title = 'MoneyTime | Tienda en línea de Calzado | Marca Puma';
        break;
      case 'reebok':
        $title = 'MoneyTime | Tienda en línea de Calzado | Marca Reebok';
        break;
      case 'rider':
        $title = 'MoneyTime | Tienda en línea de Calzado | Marca Rider';
        break;
      case 'vans':
        $title = 'MoneyTime | Tienda en línea de Calzado | Marca Vans';
        break;
      default:
        $title = $title;
        break;
    }
  }
  return $title;
}

add_filter('wpseo_opengraph_desc', 'mt_custom_seo_descriptions');
add_filter('wpseo_metadesc', 'mt_custom_seo_descriptions');
function mt_custom_seo_descriptions($desc){
  if (is_shop()) {
    $type = isset($_GET['filter_tipo']) ? $_GET['filter_tipo'] : '';
    $category = isset($_GET['product_cat']) ? $_GET['product_cat'] : '';

    switch ($type) {
      case 'mujer':
        switch ($category) {
          case 'basketball':
            $desc = 'Encesta con el mejor estilo de nuestros novedosos modelos de calzado para basketball para mujeres. Entra a MoneyTime.';
            break;
          case 'relajado':
            $desc = 'Conoce nuestros modelos más cool para mujeres. Para un Outfit Relajado visita Moneytime haciendo clic aquí.';
            break;
          case 'formal':
            $desc = '¿Necesitas un outfit serio? En Moneytime visita nuestro extenso catálogo y consigue increíbles precios para Calzado formal para mujeres.';
            break;
          case 'running':
            $desc = '¿Completa tu outfit runner aquí en MoneyTime. Los mejores tenis para salir a correr en comodidad y estilo. Consulta nuestro catálogo aquí.';
            break;
          case 'sandalias':
            $desc = 'Encuentra Sandalias y Chanclas para Mujeres en Moneytime de Nike, adidas, Reebok y más. Lo más nuevo, ofertas y descuentos en sandalias para mujer.';
            break;
          case 'training':
            $desc = 'Entrena cómodamente con tus nuevos tenis. Encuéntralos aquí en MoneyTime.';
            break;
          case 'skate':
            $desc = 'Tenis para Skate de mujer en MoneyTime. Lo más nuevo, ofertas y descuentos en marcas como Vans, DC Shoes, Nike y otras. Envío GRATIS.';
            break;
          case 'casual':
            $desc = 'Vístete de la mejor forma con los modelos de tenis casuales para mujeres que tenemos en MoneyTime. Envíos gratuitos a toda la República.';
            break;
          default:
            $desc = 'Define el mejor de tus outfits con el mejor par de tenis que encontrarás en nuestro extenso catálogo con las mejores marcas.';
            break;
        }
        break;
      case 'hombre':
        switch ($category) {
          case 'basketball':
            $desc = 'Encesta con el mejor estilo de nuestros novedosos modelos de calzado para basketball para hombres. Entra a MoneyTime.';
            break;
          case 'relajado':
            $desc = 'Conoce nuestros modelos más cool para hombres. Para un Outfit Relajado visita Moneytime haciendo clic aquí.';
            break;
          case 'formal':
            $desc = '¿Necesitas un outfit serio? En Moneytime visita nuestro extenso catálogo y consigue increíbles precios para Calzado formal para hombres.';
            break;
          case 'running':
            $desc = 'Completa tu outfit runner aquí en MoneyTime. Los mejores tenis para salir a correr en comodidad y estilo. Consulta nuestro catálogo aquí.';
            break;
          case 'sandalias':
            $desc = 'Encuentra Sandalias y Chanclas para Hombres en Moneytime de Nike, adidas, Reebok y más. Lo más nuevo, ofertas y descuentos en sandalias para hombre.';
            break;
          case 'training':
            $desc = 'Entrena cómodamente con tus nuevos tenis. Encuéntralos aquí en MoneyTime.';
            break;
          case 'skate':
            $desc = 'Tenis para Skate para hombre en MoneyTime. Lo más nuevo, ofertas y descuentos en marcas como Vans, DC Shoes, Nike y otras. Envío GRATIS.';
            break;
          case 'casual':
            $desc = 'Vístete de la mejor forma con los modelos de tenis casuales para hombres que tenemos en MoneyTime. Envíos gratuitos a toda la República.';
            break;
          default:
            $desc = 'Define el mejor de tus outfits con el mejor par de tenis que encontrarás en nuestro extenso catálogo con las mejores marcas.';
            break;
        }
        break;
      case 'juvenil':
        switch ($category) {
          case 'basketball':
            $desc = 'Encesta con el mejor estilo de nuestros novedosos modelos de calzado para basketball para niños. Entra a MoneyTime.';
            break;
          case 'relajado':
            $desc = 'Conoce nuestros modelos más cool para niños. Para un Outfit Relajado visita Moneytime haciendo clic aquí.';
            break;
          case 'formal':
            $desc = '¿Necesitas un outfit serio para tus niños? En Moneytime visita nuestro extenso catálogo y consigue increíbles precios para Calzado Formal Juvenil';
            break;
          case 'running':
            $desc = 'Completa tu outfit runner aquí en MoneyTime. Los mejores tenis para salir a correr en comodidad y estilo. Consulta nuestro catálogo aquí.';
            break;
          case 'sandalias':
            $desc = 'Encuentra Sandalias y Chanclas para Niños en Moneytime de Nike, adidas, Reebok y más. Lo más nuevo, ofertas y descuentos en sandalias para niños.';
            break;
          case 'training':
            $desc = 'Entrena cómodamente con tus nuevos tenis. Encuéntralos aquí en MoneyTime.';
            break;
          case 'skate':
            $desc = 'Tenis para Skate de niño en MoneyTime. Lo más nuevo, ofertas y descuentos en marcas como Vans, DC Shoes, Nike y otras. Envío GRATIS.';
            break;
          case 'casual':
            $desc = 'Vístete de la mejor forma con los modelos de tenis casuales para niños que tenemos en MoneyTime. Envíos gratuitos a toda la República.';
            break;
          default:
            $desc = 'Define el mejor de tus outfits con el mejor par de tenis que encontrarás en nuestro extenso catálogo con las mejores marcas.';
            break;
        }
        break;
    }

    $brand = isset($_GET['filter_brand']) ? $_GET['filter_brand'] : '';

    switch ($brand) {
      case 'adidas':
        $desc = 'Lo más nuevo de Adidas lo encuentras en MoneyTime. Conoce diversos modelos para mujer y hombre. Arma tu mejor outfit aquí.';
        break;
      case 'dc':
        $desc = 'Complemente tu estilo más urbano con lo más nuevo de DC, aquí en MoneyTime. Conoce diversos modelos para mujer y hombre. Calza con lo último en tendencia aquí.';
        break;
      case 'nike':
        $desc = 'Adquiere tus Nike en nuestra tienda en línea. Siempre disponibles y recuerda que el envío es gratuito.';
        break;
      case 'puma':
        $desc = 'Adquiere tus tenis Puma. Envíos gratuitos a toda la república. Estás a unos cuantos clics de darle a tu clóset el boost que necesitas.';
        break;
      case 'reebok':
        $desc = 'Llévate unos Reebok con envío gratis a toda la República Mexicana, Conoce nuestro catálogo y nuestras promociones.';
        break;
      case 'rider':
        $desc = '¿Buscas sandalias marca Rider? Encuentra aquí los últimos modelos al mejor precio.';
        break;
      case 'vans':
        $desc = 'La autenticidad será el punto de tu estilo con los mejores modelos de Vans que encontrarás aquí. Entra para conocer el catálogo completo.';
        break;
      default:
        $desc = $desc;
        break;
    }
  }
  return $desc;
}
