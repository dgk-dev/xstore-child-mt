<?php

//mensaje extra en recuperar password
add_action('woocommerce_after_lost_password_confirmation_message', 'mt_after_lost_password_confirmation_message');
function mt_after_lost_password_confirmation_message(){
	echo "<p><strong>Si no lo encuentras en la bandeja de entrada, busca en tu bandeja de spam o correo no deseado.</strong></p>";
}

/**
 * RFC - Razón social e intercambio de información de facturación y envío
 */
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );

add_action('woocommerce_after_order_notes', 'billing_invoice_control');
function billing_invoice_control($checkout){
    echo '<div id="billing_invoice_control">';
    
    woocommerce_form_field('billing_invoice', array(
        'type'        => 'checkbox', // add field type
        'label'       => __('¿Requiere factura?', 'woocommerce'), // Add custom field label
        'required'    => false, // if field is required or not
        'clear'       => false, // add clear or not
    ),$checkout->get_value('billing_invoice'));
    
    echo '</div>';
}

add_action( 'woocommerce_checkout_update_order_meta', 'mt_save_billing_invoice_meta' );
function mt_save_billing_invoice_meta( $order_id ){
	if( !empty( $_POST['billing_invoice'] ) )
		update_post_meta( $order_id, '_billing_invoice', sanitize_text_field( $_POST['billing_invoice'] ) );
 
}

add_filter( 'woocommerce_billing_fields', 'mt_additional_billing_fields', 10, 1 );
function mt_additional_billing_fields( $fields ){
    // echo "<pre>";
    // print_r($fields);
    // echo "</pre>";
    $fields['billing_rfc'] = array(
        'type'        => 'text', // add field type
        'label'       => __('RFC', 'woocommerce'), // Add custom field label
        'required'    => true,
        'placeholder' => _x('RFC', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'clear'       => false, // add clear or not
        'class'       => array( 'form-row-first' ),
    );
    
    $fields['billing_razon_social'] = array(
        'type'        => 'text', // add field type
        'label'       => __('Razón social', 'woocommerce'), // Add custom field label
        'placeholder' => _x('Razón social', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'clear'       => false, // add clear or not
        'class'       => array( 'form-row-first' ),

    );

    unset($fields['billing_email']);

    return  $fields;
}

add_filter( 'woocommerce_shipping_fields', 'mt_additional_shipping_fields', 10, 1 );
function mt_additional_shipping_fields( $fields ){
    $fields['shipping_phone'] = array(
        'label'        => __( 'Phone', 'woocommerce' ),
        'required'     => true,
        'type'         => 'tel',
        'class'        => array( 'form-row-wide' ),
        'autocomplete' => 'tel',
        'custom_attributes' => array('maxlength'=>'10')
    );

    $fields['billing_email'] = array(
        'label'        => __( 'Email address', 'woocommerce' ),
        'required'     => true,
        'type'         => 'email',
        'class'        => array( 'form-row-wide' ),
        'validate'     => array( 'email' ),
        'autocomplete' => 'no' === get_option( 'woocommerce_registration_generate_username' ) ? 'email' : 'email username'
    );

    return  $fields;
}

add_action('woocommerce_checkout_process', 'mt_check_billing_validation');
function mt_check_billing_validation(){
    if(!$_POST['billing_invoice']){
        $_POST['billing_first_name']= $_POST['shipping_first_name'];
        $_POST['billing_last_name']= $_POST['shipping_last_name'];
        $_POST['billing_company']= $_POST['shipping_company'];
        $_POST['billing_country']= $_POST['shipping_country'];
        $_POST['billing_address_1']= $_POST['shipping_address_1'];
        $_POST['billing_address_2']= $_POST['shipping_address_2'];
        $_POST['billing_city']= $_POST['shipping_city'];
        $_POST['billing_state']= $_POST['shipping_state'];
        $_POST['billing_postcode']= $_POST['shipping_postcode'];
        $_POST['billing_phone']= $_POST['shipping_phone'];
        $_POST['billing_rfc']= '-----';
        $_POST['billing_billing_razon_social']= '-----';
    }
}

add_filter( 'woocommerce_email_order_meta_fields', 'mt_woocommerce_email_order_send_billing_field', 10, 3 );
function mt_woocommerce_email_order_send_billing_field( $fields, $sent_to_admin, $order ) {
    $fields['send_billing'] = array(
        'label' => __( '¿Deseas recibir factura?' ),
        'value' => get_post_meta( $order->id, '_billing_invoice', true ) ? 'Sí' : 'No',
    );
    return $fields;
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'mt_display_send_billing_on_order_edit_pages', 10, 1 );
function mt_display_send_billing_on_order_edit_pages( $order ){
    $billing_invoice = get_post_meta( $order->get_id(), '_billing_invoice', true );
	$billing_text = $billing_invoice ? 'Sí' : 'No';
	echo '<p><strong>¿Deseas recibir factura?: </strong>'.$billing_text.'</p>';
    if( ! empty( $billing_invoice ) ){
		$billing_rfc = get_post_meta( $order->get_id(), '_billing_rfc', true );
		$billing_razon_social = get_post_meta( $order->get_id(), '_billing_razon_social', true );
        echo '<p><strong>RFC: </strong>'.$billing_rfc.'</p>';
        echo '<p><strong>Razón social: </strong>'.$billing_razon_social.'</p>';
    }
}

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'mt_display_shipping_phone_on_order_edit_pages', 10, 1 );
function mt_display_shipping_phone_on_order_edit_pages( $order ){
    $shipping_phone = get_post_meta( $order->get_id(), '_shipping_phone', true );
	echo '<strong>Teléfono: </strong>'.$shipping_phone;
}

/**
 * Validar teléfono a 10 dígitos
 */

add_filter( 'woocommerce_checkout_fields', 'rb_remove_default_phone_validation' );
function rb_remove_default_phone_validation( $fields ){
    unset( $fields['billing']['billing_phone']['validate'] );
	return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
    $fields['billing_phone']['maxlength'] = 10;    
    return $fields;
}
add_action('woocommerce_checkout_process', 'custom_validate_billing_shipping_phone');
function custom_validate_billing_shipping_phone() {
    $is_correct = preg_match('/^[0-9]{10}$/', $_POST['shipping_phone']);
    if ( $_POST['shipping_phone'] && !$is_correct) {
        wc_add_notice( __( 'Envío teléfono tiene que ser <strong>de 10 dígitos</strong>.' ), 'error' );
    }

    $is_correct = preg_match('/^[0-9]{10}$/', $_POST['billing_phone']);
    if (!empty($_POST['billing_invoice']) && $_POST['billing_phone'] && !$is_correct) {
        wc_add_notice( __( 'Facturación teléfono tiene que ser <strong>de 10 dígitos</strong>.' ), 'error' );
    }
}

/** Filtro promociones */
add_filter( 'pre_get_posts', 'mt_catalog_filters' );
function mt_catalog_filters( $query ) {
    if ( $query->is_main_query() && $query->post_type = 'product' && isset($_GET['on_sale']) && $_GET['on_sale'] == 1)
        $query->set('post__in', array_merge( array( 0 ), wc_get_product_ids_on_sale() ));
    
    return $query;
}

add_filter('page_link','add_on_sale_query_var');
function add_on_sale_query_var( $link ) {
    $link = isset($_GET['on_sale']) && strpos($link, '/tienda') !== false ? add_query_arg( 'on_sale', $_GET['on_sale'], $link ) : $link;
    return $link;
}

add_filter('pre_option_woocommerce_tax_display_cart', 'mt_override_tax_display_setting');
function mt_override_tax_display_setting() {
    if ( is_cart() || is_checkout()) {
        return "excl";
    } else {
        return "incl";
    }
}

add_action('woocommerce_process_registration_errors', 'mt_validate_register', 10, 2 );
function mt_validate_register( $errors, $user ) {
	// change value here to set minimum required password chars
    if(strlen($_POST['password']) != 7  ) {
    	$errors->add( 'woocommerce_password_error', __( 'La contraseña debe ser de 7 caracteres.' ) );
    }
    // adding ability to set maximum allowed password chars -- uncomment the following two (2) lines to enable that
    //elseif (strlen($_POST['password']) > 16 )
     	//$errors->add( 'woocommerce_password_error', __( 'Password must be shorter than 16 characters.' ) );
    return $errors;
}

add_action('woocommerce_save_account_details_errors', 'mt_validate_profile_update', 10, 2 );
function mt_validate_profile_update( $errors, $user ) {
	// change value here to set minimum required password chars
    if(strlen($_POST['password_2']) != 7  ) {
    	$errors->add( 'woocommerce_password_error', __( 'La contraseña debe ser de 7 caracteres.' ) );
	}
	// adding ability to set maximum allowed password chars -- uncomment the following two (2) lines to enable that
    //elseif (strlen($_POST['password_2']) > 16 )
     	//$errors->add( 'woocommerce_password_error', __( 'Password must be shorter than 16 characters.' ) );
    return $errors;
}

add_action('woocommerce_password_reset', 'mt_validate_password_reset', 10, 2 );
function mt_validate_password_reset( $errors, $user ) {
	// change value here to set minimum required password chars -- uncomment the following two (2) lines to enable that
    if(strlen($_POST['password_3']) != 7  ) {
    	$errors->add( 'woocommerce_password_error', __( 'La contraseña debe ser de 7 caracteres.' ) );
	}
	// adding ability to set maximum allowed password chars -- uncomment the following two (2) lines to enable that
    //elseif (strlen($_POST['password_3']) > 16 )
     	//$errors->add( 'woocommerce_password_error', __( 'Password must be shorter than 16 characters.' ) );
    return $errors;
}

add_action( 'woocommerce_after_checkout_validation', 'mt_validate_checkout_register', 10, 2 );
function mt_validate_checkout_register( $user ) {
	// change value here to set minimum required password chars on checkout page account registration
    if (isset($_POST['account_password']) && strlen( $_POST['account_password'] ) != 7  ) {
    	wc_add_notice( __( 'La contraseña debe ser de 7 caracteres.', 'woocommerce' ), 'error' );
	}
}

// Desactivar generación de miniaturas
add_filter( 'woocommerce_background_image_regeneration', '__return_false' );