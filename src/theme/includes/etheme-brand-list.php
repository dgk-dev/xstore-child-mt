<?php
namespace ETC\App\Controllers\Shortcodes;

use ETC\App\Controllers\Shortcodes;

/**
 * Brands List shortcode.
 *
 * @since      1.4.4
 * @package    ETC
 * @subpackage ETC/Models/Admin
 */
class Brands_List extends Shortcodes {

	public function hooks() {}

	function brands_list_shortcode( $atts ) {

		if ( ! function_exists( 'etheme_woocommerce_notice' ) || etheme_woocommerce_notice() )
			return;

		if ( ! etheme_get_option( 'enable_brands' ) )
			return '<div class="woocommerce-info">'.esc_html__('Enable brands in Theme options -> Shop elements -> Brands to use this element', 'xstore-core').'</div>';

		$atts = shortcode_atts( array(
			'ids'               	=>	false,
			'columns'           	=>	'',
			'hide_a_z'          	=>	false,
			'alignment'         	=>	'left',
			'capital_letter'    	=>	false,
			'display_type'      	=>	'',
			'brand_title'       	=>	'yes',
			'brand_image'       	=>	false,
			'brand_desc'        	=>	false,
			'tooltip'           	=>	false,
			'hide_empty'        	=>	false,
			'show_product_counts' 	=>	false,
			'size' 					=>	'',
			'class'      			=>	'',
			'is_preview'			=> false
		), $atts );

		$options = array();

		$options['terms_args'] = array(
			'hide_empty' => $atts['hide_empty']
		);

		if ( $atts['ids'] )
			$options['terms_args']['include'] = explode( ',' , $atts['ids'] );

		$options['product_brands'] = get_terms( 'brand', $options['terms_args'] );

		$options['brands'] = array();

		foreach ( $options['product_brands'] as $brand ) {

			$options['firstLetter'] = strtoupper( mb_substr( $brand->name, 0, 1, 'UTF-8' ) );

			$options['brand_obj'] = (object)array(
				'id' => $brand->term_id,
				'name' => $brand->name,
				'desc' => $brand->description,
				'count' => $brand->count
			);

			$options['brands'][$options['firstLetter']][] = $options['brand_obj'];

			unset($options['firstLetter']);
			unset($options['brand_obj']);
		}

		if ( ! $atts['size'] )
			$atts['size'] = array(50,50);
		elseif ( strpos($atts['size'], 'x') != false )
			$atts['size'] = explode( 'x', $atts['size'] );

		ob_start();

		?>

		<div class="container brands-list rb-brand-list">

			<div class="row brand-list">
        
            <?php $current_letter = ''; $counter=0; $open_row = false;?>
            <?php foreach ( $options['product_brands'] as $brand ) {
                    $counter -= -1;
					$options['class'] = '';
					$brand_letter = strtoupper( mb_substr( $brand->name, 0, 1, 'UTF-8' ) );
					
					
					switch ( $atts['columns'] ) {
						case 1:
						$options['class'] = 'col-md-12 col-sm-12 col-xs-12';
						break;
						case 2:
						$options['class'] = 'col-md-6 col-sm-6 col-xs-12';
						break;
						case 3:
						$options['class'] = 'col-md-4 col-sm-4 col-xs-12';
						break;
						case 4:
						$options['class'] = 'col-md-3 col-sm-3 col-xs-12';
						break;
						case 5:
						$options['class'] = 'еt_col-5 col-sm-3 col-xs-12';
						break;
						case 6:
						$options['class'] = 'col-md-2 col-sm-3 col-xs-12';
						break;
						default:
					}

					?>

					<?php if($atts['capital_letter'] && $current_letter != $brand_letter): ?>
						<?php if($open_row): ?>
							</div>
						<?php endif; ?>
						<div class="row <?php echo $brand_letter; ?>">
							<div class="col-xs-12 capital-letter">
								<?php echo $brand_letter; ?>
							</div>
						</div>
					<?php $counter=1; $current_letter=$brand_letter; endif; ?>
					
					
					<?php if(($counter % $atts['columns'] == 1)): ?>
						<div class="row">
					<?php $open_row = true; endif; ?>
					

                    <div class="et-isotope-item brand-list-item <?php echo esc_attr($options['class']).' '.$brand_letter; ?>">
                        <div class="work-item text-<?php echo esc_attr($atts['alignment']); ?>">
                            
                            <?php
                                $options['brand_url'] = get_permalink( wc_get_page_id( 'shop' ) ).'?orderby=date&filter_brand='.$brand->slug;;
								$options['thumb_id'] = absint( get_term_meta( $brand->term_id, 'thumbnail_id', true ) );
								$options['image'] = wp_get_attachment_image_url( $options['thumb_id'], $atts['size'] );
								if ( $atts['brand_image'] && $options['image'] ) { ?>

									<a class="brand-image" href="<?php echo $options['brand_url']; ?>" style="background-image : url('<?php echo esc_url($options['image']); ?>')">
									</a>

								<?php } ?>

								<div class="vertical-align full">

									<a href="<?php echo $options['brand_url']; ?>" class="brand-desc">

										<?php if ( $atts['brand_title'] ) : ?>

											<h4 class="title">

												<?php

												echo esc_html($brand->name);

												if ( $atts['show_product_counts'] )
													echo '<span class="colorGrey">' . esc_html('('.$brand->count.')') . '</span>';

												if ( $atts['tooltip'] && $options['image'] ) { ?>

													<div class="brand-tooltip">

														<img src="<?php echo esc_url($options['image']); ?>" alt="<?php echo esc_attr($brand->name); ?>" class="brand-img-tooltip">
														<div class="sub-title colorGrey">
															<?php echo wp_specialchars_decode($brand->desc); ?>
														</div>

													</div>

												<?php } ?>
											</h4>

										<?php endif; ?>

									</a>

									<?php if ( $atts['brand_desc'] )
										echo '<div class="sub-title">' . wp_specialchars_decode($brand->desc) . '</div>';
									?>
								</div>

							</div> <?php // .work-item ?>

				
					</div> <?php // .portfolio-item ?>

					<?php if($counter % $atts['columns'] == 0): $open_row = false;?>
						</div>
					<?php endif; ?>

				<?php } ?>
                <?php if($open_row):?>
                    </div>
                <?php endif; ?>
			</div> <?php // .brand-list ?>

		</div>	<?php // .container ?>

		<?php
		
		if ( $atts['is_preview'] )
			echo parent::initPreviewJs();

		unset($atts);
		unset($options);

		return ob_get_clean();
	
	}
}