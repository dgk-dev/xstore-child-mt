<?php
/**
 * Sobreescribir la función original para obtener productos, agregando un filtro para setear parámetros especiales para los grids de productos nuevos con offset y últimos vendidos
 */

function etheme_products($args,$title = false, $columns = 4, $extra = array() ){
    global $wpdb, $woocommerce_loop;
    $output = '';
// TODO check
//        if ( isset($woocommerce_loop['view_mode']) && $woocommerce_loop['view_mode'] == 'list' && $columns > 3) { $columns = 3; }

    if ( isset( $extra['navigation'] ) && $extra['navigation'] != 'off' ){
        $args['no_found_rows'] = false;
        $args['posts_per_page'] = $extra['per-page'];
    } 
    $args = apply_filters( 'etheme_set_products_args', $args, $title );
    $products = new WP_Query( $args );
    $class = '';

   

    wc_setup_loop( array(
        'columns'      => $columns,
        'name'         => 'product',
        'is_shortcode' => true,
        'total'        => $args['posts_per_page'],
    ) );

    if ( $products->have_posts() ) :  
        if ( wc_get_loop_prop( 'total' ) ) {
        ?>
        <?php woocommerce_product_loop_start(); ?>

            <?php while ( $products->have_posts() ) : $products->the_post(); ?>

               <?php $output .= wc_get_template_part( 'content', 'product' ); ?>

            <?php endwhile; // end of the loop. ?>
            
        <?php woocommerce_product_loop_end(); ?>
        <?php } ?>
    <?php endif;

    wp_reset_postdata();
    wc_reset_loop();

    // ! Do it for load more button
    if ( isset( $extra['navigation'] ) && $extra['navigation'] != 'off' ) {
        if ( $products->max_num_pages > 1 && $extra['limit'] > $extra['per-page'] ) {
            $attr = 'paged="1"';
            $attr .= ' max-paged="' . $products->max_num_pages . '"';

            if ( isset( $extra['limit'] ) && $extra['limit'] != -1 ) {
                $attr .= ' limit="' . $extra['limit'] . '"';
            }

            $ajax_nonce = wp_create_nonce( 'etheme_products' );

            $attr .= ' nonce="' . $ajax_nonce . '"';

            $loader = etheme_loader(false);
            $type = ( $extra['navigation'] == 'lazy' ) ? 'lazy-loading' : 'button-loading';

            $output .= '
            <div class="et-load-block text-center et_load-products ' . $type . '">
                ' . $loader . '
                <span class="btn">
                    <a ' . $attr . '>' . esc_html__( 'Load More', 'xstore' ) . '</a>
                </span>
            </div>';
        }
    }
    return $output;
}

add_filter('etheme_set_products_args', 'mt_set_male_products_grid', 10, 2);
function mt_set_male_products_grid($args, $title){
    if($title != 'mt_male_products') return $args;
    
    $args['tax_query'][]=array(
        'taxonomy' => 'pa_genero',
            'field' => 'slug',
            'terms' => array('hombre'),
    );

    return $args;
}
add_filter('etheme_set_products_args', 'mt_set_female_products_grid', 10, 2);
function mt_set_female_products_grid($args, $title){
    if($title != 'mt_female_products') return $args;
    
    $args['tax_query'][]=array(
        'taxonomy' => 'pa_genero',
            'field' => 'slug',
            'terms' => array('mujer'),
    );

    return $args;
}